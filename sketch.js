function drawBars(
  ctx,
  dataArray,
  barSettings,
  circSettings
) {
  const circLineTo = (x, y) =>
    ctx.lineTo(...mapCircle(circSettings)(x, y));

  ctx.lineCap = barSettings.lineCap;
  ctx.lineJoin = barSettings.lineJoin;
  ctx.miterLimit = barSettings.lineMiterLimit;
  ctx.lineWidth = barSettings.lineWidth;
  if (barSettings.drawMode !== "bars") ctx.beginPath();
  dataArray.forEach((amp, i) => {
    const posX = map(
      (i * 2) / (dataArray.length - 1) - 1 + barSettings.posX,
      -1 / barSettings.widthMultiplier,
      1 / barSettings.widthMultiplier,
      0,
      ctx.canvas.width
    );

    if (barSettings.drawMode === "bars") {
      ctx.lineWidth = Math.max(
        (ctx.canvas.width / (dataArray.length - 1)) * barSettings.width * barSettings.widthMultiplier,
        1
      );
      ctx.beginPath();
    }
    if (barSettings.drawMode === "fill" && i <= 0)
      circLineTo(
        posX,
        ctx.canvas.height / 2 + (ctx.canvas.height * barSettings.posY) / 2 + barSettings.fixedHeight / 2
      );
    circLineTo(
      posX,
      -amp * barSettings.heightMultiplier * ctx.canvas.height +
        ctx.canvas.height / 2 +
        (ctx.canvas.height * barSettings.posY) / 2 -
        barSettings.fixedHeight / 2
    );
    if (barSettings.drawMode === "fill" && i >= dataArray.length - 1)
      circLineTo(
        posX,
        ctx.canvas.height / 2 + (ctx.canvas.height * barSettings.posY) / 2 + barSettings.fixedHeight / 2
      );
    if (barSettings.drawMode === "bars") {
      circLineTo(
        posX,
        ctx.canvas.height / 2 + (ctx.canvas.height * barSettings.posY) / 2 + barSettings.fixedHeight / 2
        // + amp * barSettings.heightMultiplier * ctx.canvas.height * (barSettings.mirror ? 1 : 0)
      );
      ctx.stroke();
    }
  });

  if (barSettings.drawMode !== "bars" && barSettings.mirror) {
    if (barSettings.drawMode === "fill") ctx.fill();
    else ctx.stroke();
    ctx.beginPath();
    
    dataArray.forEach((amp, i) => {
      const posX = map(
        (i * 2) / (dataArray.length - 1) - 1 + barSettings.posX,
        -1 / barSettings.widthMultiplier,
        1 / barSettings.widthMultiplier,
        0,
        ctx.canvas.width
      );
      if (barSettings.drawMode === "fill" && i <= 0)
        circLineTo(posX, ctx.canvas.height / 2 + (ctx.canvas.height * barSettings.posY) / 2);
      circLineTo(
        posX,
        amp * barSettings.heightMultiplier * ctx.canvas.height +
          ctx.canvas.height / 2 +
          (ctx.canvas.height * barSettings.posY) / 2 +
          barSettings.fixedHeight / 2
      );
      if (barSettings.drawMode === "fill" && i >= dataArray.length - 1)
        circLineTo(posX, ctx.canvas.height / 2 + (ctx.canvas.height * barSettings.posY) / 2);
    });
  }
  if (barSettings.drawMode !== "bars") {
    if (barSettings.drawMode === "fill") ctx.fill();
    else ctx.stroke();
  }
}

function drawSpectrum(
  ctx,
  audioCtx,
  fftSize,
  waveform,
  dataArray,
  spectrumSettings,
  barSettings,
  circSettings
) {
  // Bump mechanic for the circle I made myself
  /*
  let coll = 0;
  dataArray.forEach((v) => (coll += v));
  coll /= dataArray.length;
  coll *= barSettings.bumpMultiplier;
  barSettings.fixedHeight = coll;
  */
  
  const bufferSize = aps.sampleProvider === "spectrum" ? aps.fftSize : aps.inputSize,
    waveformSize = aps.sampleProvider === "spectrum" ? Math.min(aps.fftSize, aps.inputSize) : aps.inputSize,
    freqBands = generateFreqBands(
      spectrumSettings.sampleOutCount,
      spectrumSettings.lowerHz,
      spectrumSettings.higherHz,
      spectrumSettings.frequencyScale,
      spectrumSettings.hzLinearFactor
    ),
    sampleRate = audioCtx.sampleRate / (-aps.hqac + 2),
    isSpectrum =
      aps.sampleProvider === "spectrum" || aps.sampleProvider === "spectrum2" || aps.sampleProvider === "freq kernel";
  let audioBuffer = new Array(bufferSize).fill(0);
  let norm = 0;
  for (let i = 0; i < waveformSize; i++) {
    // Apply window function
    let x = (i * 2) / (waveformSize - 1) - 1;
    let w = applyWindow(x, winTransform.windowFunction, winTransform.windowParameter, true, winTransform.windowSkew);
    audioBuffer[i] = waveform[i + (fftSize - waveformSize)] * w;
    norm += w;
  }
  if (isSpectrum) audioBuffer = audioBuffer.map((x, _, y) => x * (y.length / norm));
  if (!aps.hqac) audioBuffer = audioBuffer.filter((_, i) => i % 2 === 0);

  let result = [];
  switch (aps.sampleProvider) {
    case "spectrum":
      const spectrum = calcFFT(audioBuffer);
      result = calcSpectrum(
        spectrum.map((x, i) => (
          x *
          calcFreqTilt(fftBinToHertz(i + 0.5), 440, spectrumSettings.perBinFrequencyTilt) *
          applyWeight(
            fftBinToHertz(i + 0.5),
            spectrumSettings.perBinWeightingAmount,
            spectrumSettings.perBinWeighting
          )
        )),
        spectrumSettings.bandpowerMode,
        spectrumSettings.useAverage,
        spectrumSettings.useRMS,
        spectrumSettings.useSum,
        spectrumSettings.fftBinInterpolation,
        spectrumSettings.interpParam,
        spectrumSettings.interpNthRoot,
        spectrumSettings.interpLogScale,
        freqBands,
        audioBuffer.length,
        sampleRate
      );
      break;

    case "freq kernel":
      const complexSpectrum = calcComplexFFT(audioBuffer);
      result = calcCQT(
        complexSpectrum,
        spectrumSettings.cqtResolution,
        !spectrumSettings.spectrumConstantQ,
        spectrumSettings.cqtBandwidth,
        winCQT.windowFunction,
        winCQT.windowParameter,
        winCQT.windowSkew,
        freqBands,
        audioBuffer.length,
        sampleRate
      );
      break;
    case "waveform":
      result = audioBuffer;
      break;
  }

  dataArray.length = result.length;
  calcSmoothingTimeConstant(
    dataArray,
    result.map(
      (x, i) =>
        x *
        dBToLinear(aps.outputMultiplier) *
        (isSpectrum
          ? calcFreqTilt(freqBands[i].ctr, 440, spectrumSettings.perBandFrequencyTilt) *
            applyWeight(freqBands[i].ctr, spectrumSettings.perBandWeightingAmount, spectrumSettings.perBandWeighting)
          : 1)
    ),
    isSpectrum ? spectrumSettings.smoothingTimeConstant : 0
  );

  let finalArray = dataArray;
  if (isSpectrum)
    finalArray = finalArray.map((x) =>
      Math.max(
        ascale(
          x,
          spectrumSettings.ampNthRoot,
          spectrumSettings.logAmplitudeScale,
          spectrumSettings.dBRange,
          spectrumSettings.useAbsValue
        ),
        0
      )
    );

  if (barSettings.mirror) {
    finalArray = finalArray.filter((_, i) => i % 2 !== 0);
    finalArray = [...finalArray].concat(...finalArray.reverse());
  }

  ctx.strokeStyle = "#ffffff";
  ctx.fillStyle = ctx.strokeStyle;

  drawBars(ctx, finalArray, barSettings, circSettings);
}

const mapCircle = (cp) => (x, y) => {
  if (!cp.circle) return [x, y];
  // Angle of the circle in radians
  let ma = cp.a + ((x - cp.xmin) * 2 * Math.PI) / (cp.xmax - cp.xmin);
  let vm = cp.rmin + y * cp.rmul;
  let vx = vm * Math.cos(ma),
    vy = -vm * Math.sin(ma);

  return [cp.x + vx, cp.y + vy];
};

//const mapCircDef = mapCircle(circSettings)
