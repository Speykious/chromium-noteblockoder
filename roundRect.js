function roundRect(x, y, w, h, r = 20) {
  const hw = w / 2,
    hh = h / 2;
  const x1 = x - hw,
    x2 = x + hw;
  const y1 = y,
    y2 = y + h;
  if (w < 2 * r) r = hw;
  if (h < 2 * r) r = hh;
  ctx.beginPath();
  ctx.moveTo(x1 + r, y1);
  ctx.arcTo(x2, y1, x2, y2, r);
  ctx.arcTo(x2, y2, x1, y2, r);
  ctx.arcTo(x1, y2, x1, y1, r);
  ctx.arcTo(x1, y1, x2, y1, r);
  ctx.closePath();
  return ctx;
}