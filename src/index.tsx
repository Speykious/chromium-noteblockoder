import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import houshouMarine from "./houshou-marine.jpg";
//import reportWebVitals from "./reportWebVitals";
import { createCanvas, loadImage, Canvas } from "canvas";
import { AudioPlayer, spectrumSettings, barSettings, circSettings } from "./spectrum/audio-settings";
import { drawSpectrum } from "./spectrum/sketch";
import Viewport from "./Viewport";
import noise from "./noise";
import {} from "";

const p = 240;
const canvas: Canvas = createCanvas((p * 16) / 9, p);
const ctx = canvas.getContext("2d");

const audio: AudioPlayer = {
  ctx: new AudioContext(),
  htmlPlayer: document.getElementById("audio") as HTMLMediaElement,
};
audio.source = audio.ctx.createMediaElementSource(audio.htmlPlayer);
audio.analyser = audio.ctx.createAnalyser();
audio.analyser.fftSize = 32768; // Set to maximum value
audio.source.connect(audio.analyser);
audio.source.connect(audio.ctx.destination);

//const waveform: Float32Array = new Float32Array(audio.analyser.fftSize);
const dataArray: number[] = [];

function roundRect(x: number, y: number, w: number, h: number, r: number = 20): CanvasRenderingContext2D {
  const hw = w / 2,
    hh = h / 2;
  const x1 = x - hw,
    x2 = x + hw;
  const y1 = y,
    y2 = y + h;
  if (w < 2 * r) r = hw;
  if (h < 2 * r) r = hh;
  ctx.beginPath();
  ctx.moveTo(x1 + r, y1);
  ctx.arcTo(x2, y1, x2, y2, r);
  ctx.arcTo(x2, y2, x1, y2, r);
  ctx.arcTo(x1, y2, x1, y1, r);
  ctx.arcTo(x1, y1, x2, y1, r);
  ctx.closePath();
  return ctx;
}

const seed1: [number, number] = [Math.random() * 42, Math.random() * 42];
const seed2: [number, number] = [Math.random() * 42, Math.random() * 42];
const seed3: [number, number] = [Math.random() * 42, Math.random() * 42];

const fileSelector = document.getElementById("file-selector") as HTMLInputElement;
fileSelector.addEventListener(
  "change",
  async () => {
    // Obtain the first uploaded file
    if (!fileSelector.files) return;
    const framerate = 60;
    const file = fileSelector.files[0];
    const image = await loadImage(houshouMarine);
    const viewport = new Viewport({ image, s: 1.2, a: 20 });
    let tick = 0;

    // Create instance of FileReader
    let reader = new FileReader();
    // When the file has been succesfully read
    reader.onload = (event) => {
      const audioCtx = new AudioContext();
      //status.innerHTML = "Decoding audio data"
      // Asynchronously decode audio file data contained in an ArrayBuffer.
      if (!event.target?.result) return;
      audioCtx.decodeAudioData(event.target.result as ArrayBuffer, (audioBuffer) => {
        const fftSize = 32768;
        const fftHalf = fftSize / 2;
        const step = audioBuffer.sampleRate / framerate;
        //progress.max = audioBuffer.duration * framerate

        //status.innerHTML = "Storing PCM data of the first channel"
        const pcmData = audioBuffer.getChannelData(0);

        let index = 0;
        let waveform: Float32Array;
        //const dataArray = []
        //status.innerHTML = "Drawing Frames"
        //capturer.start()

        function nextFrame() {
          const origoffset = -90; // Offset in milliseconds
          const morigoffset = (origoffset * audioBuffer.sampleRate) / 1000;
          waveform = pcmData.slice(index - fftHalf + morigoffset, index + fftHalf + morigoffset);
          
          const affordableTick = tick / 50;
          viewport.a = 20 * noise(affordableTick, ...seed1) - 10;
          viewport.x = 50 * noise(affordableTick, ...seed2) - 10;
          viewport.y = 20 * noise(affordableTick, ...seed3) - 10;

          const cw = ctx.canvas.width, ch = ctx.canvas.height;
          const hcw = cw >> 1, hch = ch >> 1;

          // Background image
          viewport.draw(ctx);
          // Dim the image
          ctx.fillStyle = "#00000030";
          ctx.fillRect(0, 0, cw, ch);
          // Draw the spectrum (duh)
          drawSpectrum(
            ctx,
            audioCtx,
            audio.analyser?.fftSize ?? 16384,
            waveform,
            dataArray,
            spectrumSettings,
            barSettings,
            circSettings
          );

          // Draw the noteblock platform
          ctx.fillStyle = "#ffffffcc";
          roundRect(hcw, ch * 0.755, cw / 1.75, hch / 3, cw / 40).fill();

          //capturer.capture(canvas)
          //progress.value++
          index += step;

          if (index < pcmData.length) {
            requestAnimationFrame(nextFrame);
          } else {
            //progress.value = progress.max
            //requestAnimationFrame(finalizeVideo);
          }

          ReactDOM.render(
            <React.StrictMode>
              <main className="app">
                <p>SYSTEM-CALL: GENERATE CANVAS ELEMENT</p>
                <img id="canvas" src={canvas.toDataURL()} alt="There should be a canvas appearing here." />
              </main>
            </React.StrictMode>,
            document.getElementById("root")
          );

          tick++;
        }

        nextFrame();
      });
    };

    // In case that the file couldn't be read
    reader.onerror = (event) => console.log("File Reading Crisis:", event);

    reader.readAsArrayBuffer(file);
  },
  false
);

ReactDOM.render(
  <React.StrictMode>
    <main className="app">
      <p>SYSTEM-CALL: GENERATE CANVAS ELEMENT</p>
      <img id="canvas" src={canvas.toDataURL()} alt="There should be a canvas appearing here." />
    </main>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
