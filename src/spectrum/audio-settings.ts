import { WindowType, InterpMode, WeightType, FreqScale } from "./helpers"

export interface AudioPlayer {
  ctx: AudioContext
  htmlPlayer: HTMLMediaElement
  source?: MediaElementAudioSourceNode
  analyser?: AnalyserNode
}

export type SampleProvider = "waveform" | "spectrum" | "freq kernel" | "spectrum2"
export interface AudioProviderSettings {
  sampleProvider: SampleProvider
  hqac: boolean
  outputMultiplier: number
  inputSize: number
  fftSize: number
}

export interface WindowSettings {
  windowFunction: WindowType
  windowParameter: number
  windowSkew: number
}

export interface SpectrumSettings {
  spectrumConstantQ: boolean
  cqtResolution: number
  cqtBandwidth: number
  sampleOutCount: number
  bandpowerMode: "interpolate"
  fftBinInterpolation: InterpMode
  interpParam: number
  interpLogScale: boolean
  interpNthRoot: number
  useAverage: boolean
  useRMS: boolean
  useSum: boolean
  smoothingTimeConstant: number
  perBinFrequencyTilt: number
  perBinWeighting: WeightType
  perBinWeightingAmount: number
  perBandFrequencyTilt: number
  perBandWeighting: WeightType
  perBandWeightingAmount: number
  lowerHz: number
  higherHz: number
  frequencyScale: FreqScale
  hzLinearFactor: number
  logAmplitudeScale: boolean
  dBRange: number
  ampNthRoot: number
  useAbsValue: boolean
}


export type DrawMode = "bars"  | "lines" | "fill"
export interface BarSettings {
  posX: number
  posY: number
  widthMultiplier: number
  heightMultiplier: number
  fixedHeight: number
  bumpMultiplier: number
  lineCap: CanvasLineCap
  lineJoin: CanvasLineJoin
  lineMiterLimit: number
  width: number
  drawMode: DrawMode
  lineWidth: number
  mirror: boolean
}

export interface CirclePath {
  circle: boolean
  x: number
  y: number
  xmin: number
  xmax: number
  rmin: number
  rmul: number
  a: number
}

/////////////////////////////////////
// SETTING THE SETTINGS HERE BELOW //
/////////////////////////////////////

export const aps: AudioProviderSettings = {
  sampleProvider: "freq kernel",
  hqac: true,
  outputMultiplier: 0,
  inputSize: 8196,
  fftSize: 16384
}

export const winTransform: WindowSettings = {
  windowFunction: "hann",
  windowParameter: 1,
  windowSkew: 0
}

export const winCQT: WindowSettings = {
  windowFunction: "hann",
  windowParameter: 1,
  windowSkew: 0
}

export const spectrumSettings: SpectrumSettings = {
  spectrumConstantQ: false,
  cqtResolution: 12,
  cqtBandwidth: 4,
  sampleOutCount: 64,
  bandpowerMode: "interpolate",
  fftBinInterpolation: "cubic",
  interpParam: 0,
  interpLogScale: false,
  interpNthRoot: 1,
  useAverage: true,
  useRMS: true,
  useSum: false,
  smoothingTimeConstant: 0,
  perBinFrequencyTilt: 0,
  perBinWeighting: "a",
  perBinWeightingAmount: 0,
  perBandFrequencyTilt: 0,
  perBandWeighting: "a",
  perBandWeightingAmount: 0,
  lowerHz: 20,
  higherHz: 400,
  frequencyScale: "linear",
  hzLinearFactor: 0.5,
  logAmplitudeScale: false,
  dBRange: 70,
  ampNthRoot: 1,
  useAbsValue: true
}

export const barSettings: BarSettings = {
  posX: 0,
  posY: 0.5,
  widthMultiplier: 0.5,
  heightMultiplier: 1,
  fixedHeight: 2,
  bumpMultiplier: 1000,
  lineCap: "butt",
  lineJoin: "miter",
  lineMiterLimit: 10,
  width: 0.75,
  drawMode: "bars",
  lineWidth: 1,
  mirror: false
}

export const circSettings: CirclePath = {
  circle: false,
  x: 400, y: 225, a: Math.PI / 2,
  xmin: 0, xmax: 800,
  rmin: 100, rmul: 0.5
}