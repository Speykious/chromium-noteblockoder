class Viewport {
  constructor(props) {
    this.image = props.image;
    this.x = props.x ?? 0;
    this.y = props.y ?? 0;
    this.s = props.s ?? 1;
    this.a = props.a ?? 0;
  }

  draw(ctx) {
    ctx.save();
    
    const cw = ctx.canvas.width;
    const ch = ctx.canvas.height;
    const hcw = cw / 2;
    const hch = ch / 2;
    const w = this.image.width;
    const h = this.image.height;
    const hw = w / 2;
    const hh = h / 2;
    ctx.translate(hcw - this.x, hch - this.y);
    ctx.rotate(this.a * Math.PI / 180);
    ctx.scale(this.s, this.s);
    ctx.drawImage(
      this.image,
      -hw, -hh, w, h
    );

    ctx.restore();
  }
}