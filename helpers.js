// p5.js map function
const map = (x, min, max, targetMin, targetMax) =>
  ((x - min) / (max - min)) * (targetMax - targetMin) + targetMin;

const clamp = (x, min, max) => Math.min(Math.max(x, min), max);

const sum = (arr) => arr.reduce((a, b) => a + b);
const average = (arr) => {
  const l = arr.length;
  return sum(arr) / l;
};

const idxWrapOver = (x, length) => ((x % length) + length) % length;

function getClippedIdx(x, length, mode = "clip") {
  switch (mode) {
    case "clip":
      return clamp(x, 0, length - 1);
    case "periodic":
      return idxWrapOver(x, length);
    case "mirror":
      const period = 2 * (length - 1);
      const i = idxWrapOver(x, period);
      return i > length - 1 ? period - i : i;
  }
}

// dB convertion
const linearTodB = (x) => 20 * Math.log10(x);
const dBToLinear = (x) => 10 ** (x / 20);

// Linear interpolation (used for FFT bin interpolation)
const lerp = (x, y, z) => x * (1 - z) + y * z;

function cubicInterp(x, y, z, w, i, tension = 0) {
  const tangentFactor = 1 - tension,
    m1 = (tangentFactor * (z - x)) / 2,
    m2 = (tangentFactor * (w - y)) / 2,
    squared = i ** 2,
    cubed = i ** 3;
  return (
    (2 * cubed - 3 * squared + 1) * y +
    (cubed - 2 * squared + i) * m1 +
    (-2 * cubed + 3 * squared) * z +
    (cubed - squared) * m2
  );
}

function interp(
  arr,
  x,
  interpMode,
  interpParameter = 0,
  nthRoot = 1,
  logarithmic = false,
  clipMode = "clip"
) {
  const intX = Math.trunc(x);
  switch (interpMode) {
    case "nearest neighbor":
      return arr[getClippedIdx(Math.round(x), arr.length, clipMode)];
    case "cubic":
      return absInvAscale(
        cubicInterp(
          absAscale(
            arr[getClippedIdx(intX - 1, arr.length, clipMode)],
            nthRoot,
            logarithmic
          ),
          absAscale(
            arr[getClippedIdx(intX, arr.length, clipMode)],
            nthRoot,
            logarithmic
          ),
          absAscale(
            arr[getClippedIdx(intX + 1, arr.length, clipMode)],
            nthRoot,
            logarithmic
          ),
          absAscale(
            arr[getClippedIdx(intX + 2, arr.length, clipMode)],
            nthRoot,
            logarithmic
          ),
          x - intX,
          interpParameter
        ),
        nthRoot,
        logarithmic
      );
    default:
      return absInvAscale(
        lerp(
          absAscale(
            arr[getClippedIdx(intX, arr.length, clipMode)],
            nthRoot,
            logarithmic
          ),
          absAscale(
            arr[getClippedIdx(intX + 1, arr.length, clipMode)],
            nthRoot,
            logarithmic
          ),
          x - intX
        ),
        nthRoot,
        logarithmic
      );
  }
}

function calcSmoothingTimeConstant(targetArr, sourceArr, factor = 0.5) {
  for (let i = 0; i < targetArr.length; i++)
    targetArr[i] =
      (isNaN(targetArr[i]) ? 0 : targetArr[i]) * factor +
      (isNaN(sourceArr[i]) ? 0 : sourceArr[i]) * (1 - factor);
}

const calcFreqTilt = (x, centerFreq = 440, amount = 3) =>
  Math.abs(amount) > 0 ? 10 ** ((Math.log2(x / centerFreq) * amount) / 20) : 1;

function applyWeight(x, weightAmount = 1, weightType = "a") {
  const f2 = x ** 2;
  switch (weightType) {
    case "a":
      return (
        ((1.2588966 * 148840000 * f2 ** 2) /
          ((f2 + 424.36) *
            Math.sqrt((f2 + 11599.29) * (f2 + 544496.41)) *
            (f2 + 148840000))) **
        weightAmount
      );
    case "b":
      return (
        ((1.019764760044717 * 148840000 * x ** 3) /
          ((f2 + 424.36) * Math.sqrt(f2 + 25122.25) * (f2 + 148840000))) **
        weightAmount
      );
    case "c":
      return (
        ((1.0069316688518042 * 148840000 * f2) /
          ((f2 + 424.36) * (f2 + 148840000))) **
        weightAmount
      );
    case "d":
      return (
        ((x / 6.8966888496476e-5) *
          Math.sqrt(
            ((1037918.48 - f2) * (1037918.48 - f2) + 1080768.16 * f2) /
              ((9837328 - f2) * (9837328 - f2) + 11723776 * f2) /
              ((f2 + 79919.29) * (f2 + 1345600))
          )) **
        weightAmount
      );
    case "m":
      const h1 =
          -4.737338981378384e-24 * f2 ** 3 +
          2.043828333606125e-15 * f2 ** 2 -
          1.363894795463638e-7 * f2 +
          1,
        h2 =
          1.306612257412824e-19 * x ** 5 -
          2.118150887518656e-11 * x ** 3 +
          5.559488023498642e-4 * x;

      return (
        ((8.128305161640991 * 1.246332637532143e-4 * x) / Math.hypot(h1, h2)) **
        weightAmount
      );
    default:
      return 1;
  }
}

/**
 * Apply window function
 * The customizable window function in Vizzy.io?
 * @param x The position of the window function from -1 to 1
 * @param windowType The specified window function to use
 * @param windowParameter The parameter of the window function (Adjustable window functions only)
 * @param truncate Zeroes out if x is more than 1 or less than -1
 * @param windowSkew Skew the window function to make symmetric windows asymmetric
 * @returns The gain of window function
 */
function applyWindow(
  posX,
  windowType = "hann",
  windowParameter = 1,
  truncate = true,
  windowSkew = 0
) {
  let x =
    windowSkew > 0
      ? ((posX / 2 - 0.5) /
          (1 - (posX / 2 - 0.5) * 10 * windowSkew ** 2) /
          (1 / (1 + 10 * windowSkew ** 2))) *
          2 +
        1
      : ((posX / 2 + 0.5) /
          (1 + (posX / 2 + 0.5) * 10 * windowSkew ** 2) /
          (1 / (1 + 10 * windowSkew ** 2))) *
          2 -
        1;

  if (truncate && Math.abs(x) > 1) return 0;

  switch (windowType.toLowerCase()) {
    default:
      return 1;
    case "hanning":
    case "cosine squared":
    case "hann":
      return Math.cos((x * Math.PI) / 2) ** 2;
    case "raised cosine":
    case "hamming":
      return 0.54 + 0.46 * Math.cos(x * Math.PI);
    case "power of sine":
      return Math.cos((x * Math.PI) / 2) ** windowParameter;
    case "tapered cosine":
    case "tukey":
      return Math.abs(x) <= 1 - windowParameter
        ? 1
        : x > 0
        ? (-Math.sin(((x - 1) * Math.PI) / windowParameter / 2)) ** 2
        : Math.sin(((x + 1) * Math.PI) / windowParameter / 2) ** 2;
    case "blackman":
      return (
        0.42 + 0.5 * Math.cos(x * Math.PI) + 0.08 * Math.cos(x * Math.PI * 2)
      );
    case "nuttall":
      return (
        0.355768 +
        0.487396 * Math.cos(1 * x * Math.PI) +
        0.144232 * Math.cos(2 * x * Math.PI) +
        0.012604 * Math.cos(3 * x * Math.PI)
      );
    case "kaiser":
      return (
        Math.cosh(Math.sqrt(1 - x ** 2) * windowParameter ** 2) /
        Math.cosh(windowParameter ** 2)
      );
    case "gauss":
    case "gaussian":
      return Math.exp(-(windowParameter ** 2) * x ** 2);
    case "bartlett":
    case "triangle":
    case "triangular":
      return 1 - Math.abs(x);
    case "quadratic spline":
      return Math.abs(x) <= 0.5
        ? -((x * Math.sqrt(2)) ** 2) + 1
        : (Math.abs(x * Math.sqrt(2)) - Math.sqrt(2)) ** 2;
    case "welch":
      return 1 - x ** 2;
  }
}

/**
 * Frequency scaling
 */
function fscale(x, freqScale = "logarithmic", freqSkew = 0.5) {
  switch (freqScale.toLowerCase()) {
    default:
      return x;
    case "log":
    case "logarithmic":
      return Math.log2(x);
    case "mel":
      return Math.log2(1 + x / 700);
    case "critical bands":
    case "bark":
      return (26.81 * x) / (1960 + x) - 0.53;
    case "equivalent rectangular bandwidth":
    case "erb":
      return Math.log2(1 + 0.00437 * x);
    case "sinh":
    case "arcsinh":
    case "asinh":
      return Math.asinh(x / 10 ** (freqSkew * 4));
    case "nth root":
      return x ** (1 / (11 - freqSkew * 10));
    case "negative exponential":
      return -(2 ** (-x / 2 ** (7 + freqSkew * 8)));
  }
}

function invFscale(x, freqScale = "logarithmic", freqSkew = 0.5) {
  switch (freqScale.toLowerCase()) {
    default:
      return x;
    case "log":
    case "logarithmic":
      return 2 ** x;
    case "mel":
      return 700 * (2 ** x - 1);
    case "critical bands":
    case "bark":
      return 1960 / (26.81 / (x + 0.53) - 1);
    case "equivalent rectangular bandwidth":
    case "erb":
      return (1 / 0.00437) * (2 ** x - 1);
    case "sinh":
    case "arcsinh":
    case "asinh":
      return Math.sinh(x) * 10 ** (freqSkew * 4);
    case "nth root":
      return x ** (11 - freqSkew * 10);
    case "negative exponential":
      return -Math.log2(-x) * 2 ** (7 + freqSkew * 8);
  }
}

// Amplitude scaling
function ascale(
  x,
  nthRoot = 1,
  logarithmic = false,
  dBRange = 70,
  useAbsoluteValue = true
) {
  if (logarithmic) return map(20 * Math.log10(x), -dBRange, 0, 0, 1);
  else
    return map(
      x ** (1 / nthRoot),
      useAbsoluteValue ? 0 : dBToLinear(-dBRange) ** (1 / nthRoot),
      1,
      0,
      1
    );
}

function absAscale(x, nthRoot = 1, logarithmic = false) {
  if (logarithmic) return 20 * Math.log10(x);
  else return x ** (1 / nthRoot);
}

function absInvAscale(x, nthRoot = 1, logarithmic = false) {
  if (logarithmic) return 10 ** (x / 20);
  else return x ** nthRoot;
}

// Hz and FFT bin conversion
function hertzToFFTBin(
  x,
  func = "round",
  bufferSize = 4096,
  sampleRate = 44100
) {
  const bin = (x * bufferSize) / sampleRate;

  if (!["floor", "ceil", "trunc"].includes(func)) func = "round"; // always use round if you specify an invalid/undefined value

  return Math[func](bin);
}

function fftBinToHertz(x, bufferSize = 4096, sampleRate = 44100) {
  return (x * sampleRate) / bufferSize;
}

// Frequency bands generator
function generateFreqBands(
  N = 128,
  low = 20,
  high = 20000,
  freqScale = "logarithmic",
  freqSkew = 0.5,
  bandwidth = 0.5
) {
  let freqArray = [];
  for (let i = 0; i < N; i++) {
    freqArray.push({
      lo: invFscale(
        map(
          i - bandwidth,
          0,
          N - 1,
          fscale(low, freqScale, freqSkew),
          fscale(high, freqScale, freqSkew)
        ),
        freqScale,
        freqSkew
      ),
      ctr: invFscale(
        map(
          i,
          0,
          N - 1,
          fscale(low, freqScale, freqSkew),
          fscale(high, freqScale, freqSkew)
        ),
        freqScale,
        freqSkew
      ),
      hi: invFscale(
        map(
          i + bandwidth,
          0,
          N - 1,
          fscale(low, freqScale, freqSkew),
          fscale(high, freqScale, freqSkew)
        ),
        freqScale,
        freqSkew
      )
    });
  }
  return freqArray;
}

function generateOctaveBands(
  bandsPerOctave = 12,
  lowerNote = 4,
  higherNote = 124,
  detune = 0,
  bandwidth = 0.5
) {
  const root24 = 2 ** (1 / 24);
  const c0 = 440 * root24 ** -114; // ~16.35 Hz
  const groupNotes = 24 / bandsPerOctave;
  let bands = [];
  for (
    let i = Math.round((lowerNote * 2) / groupNotes);
    i <= Math.round((higherNote * 2) / groupNotes);
    i++
  ) {
    bands.push({
      lo: c0 * root24 ** ((i - bandwidth) * groupNotes + detune),
      ctr: c0 * root24 ** (i * groupNotes + detune),
      hi: c0 * root24 ** ((i + bandwidth) * groupNotes + detune)
    });
  }
  return bands;
}

// Calculate the FFT
function calcFFT(input) {
  let fft = input.map((x) => x);
  let fft2 = input.map((x) => x);
  transform(fft, fft2);
  let output = new Array(Math.round(fft.length / 2)).fill(0);
  for (let i = 0; i < output.length; i++)
    output[i] = Math.hypot(fft[i], fft2[i]) / fft.length;
  return output;
}

function calcComplexFFT(input) {
  let fft = input.map((x) => x);
  let fft2 = input.map((x) => x);
  transform(fft, fft2);
  return input.map((_, i, arr) => ({
    re: fft[i] / (arr.length / 2),
    im: fft2[i] / (arr.length / 2),
    magnitude: Math.hypot(fft[i], fft2[i]) / (arr.length / 2),
    phase: Math.atan2(fft2[i], fft[i])
  }));
}

function calcGoertzel(waveform, coeff) {
  let firstPrev = 0,
    secondPrev = 0,
    result = 0;
  waveform.forEach((x) => {
    let f1 = firstPrev,
      f2 = secondPrev;
    const sine = x + coeff * f1 - f2;
    f2 = f1;
    f1 = sine;
    result = Math.sqrt(f2 ** 2 + f1 ** 2 - coeff * f1 * f2) / waveform.length;
    firstPrev = f1;
    secondPrev = f2;
  });
  return result;
}

function calcGoertzel2(waveform, coeff) {
  let f1 = 0,
    f2 = 0;
  waveform.forEach((x) => {
    const sine = x + coeff * f1 - f2;
    f2 = f1;
    f1 = sine;
  });
  return Math.sqrt(f2 ** 2 + f1 ** 2 - coeff * f1 * f2) / waveform.length;
}

function calcBandpower(
  fftCoeffs,
  dataIdx,
  endIdx,
  average = true,
  useRMS = true,
  sum = false,
  interpolate = false,
  interpMode = "linear",
  interpParam = 0,
  interpNthRoot = 1,
  interpLogScale = false
) {
  let amp = 0;
  const low = clamp(dataIdx, 0, fftCoeffs.length - 1),
    high = clamp(endIdx, 0, fftCoeffs.length - 1),
    diff = endIdx - dataIdx + 1;
  if (interpolate) {
    const xFloat = low,
      xInt = Math.trunc(xFloat);
    if (xFloat - xInt <= 0) amp = fftCoeffs[dataIdx];
    else
      amp = interp(
        fftCoeffs,
        xFloat,
        interpMode,
        interpParam,
        interpNthRoot,
        interpLogScale
      );
  } else {
    for (let i = low; i <= high; i++) {
      if (average) amp += fftCoeffs[i] ** (useRMS ? 2 : 1);
      else amp = Math.max(amp, fftCoeffs[i]);
    }
    if (average)
      amp = useRMS ? Math.sqrt(amp / (sum ? 1 : diff)) : amp / (sum ? 1 : diff);
  }
  return amp;
}

/**
 * Calculate frequency bands power spectrum from FFT
 *
 * @param {number[]} fftCoeffs - The array of FFT magnitude coefficients
 * @param {string} [mode=interpolate] - FFT to frequency bands spectrum mode, setting to interpolate to do FFT bin interpolation, otherwise to insert zeroes, or bandpower to get average amount of amplitude between 2 frequency boundaries
 * @param {boolean} [average=true] - Average to sum FFT coefficients into bandpower coefficient
 * @param {FrequencyBands[]} [hzArray=generateFreqBands()] - The array of frequency bands in Hz
 * @returns {number[]} Output of frequency bands spectrum from linearly-spaced FFT coefficients
 */
function calcSpectrum(
  fftCoeffs,
  mode = "interpolate",
  average = true,
  useRMS = true,
  sum = false,
  fftBinInterpolation,
  interpParam,
  interpNthRoot,
  interpLogScale,
  hzArray = generateFreqBands(),
  bufferSize = fftCoeffs.length * 2,
  sampleRate = 44100
) {
  let boundArr = [];
  let nBars = 0;
  for (let i = 0; i < hzArray.length; i++) {
    let minIdx = hertzToFFTBin(
      hzArray[i].lo,
      mode === "bandpower" ? "round" : "ceil",
      bufferSize,
      sampleRate
    );
    let maxIdx = hertzToFFTBin(
      hzArray[i].hi,
      mode === "bandpower" ? "round" : "floor",
      bufferSize,
      sampleRate
    );
    let minIdx2 = hertzToFFTBin(hzArray[i].lo, "floor", bufferSize, sampleRate);
    //console.log(minIdx, maxIdx, minIdx2);
    if (mode === "interpolate" && fftBinInterpolation !== "zero insertion") {
      if (minIdx > maxIdx) nBars++;
      else {
        if (nBars > 1) {
          //console.log("nBars:", nBars, "length:", boundArr.length);
          for (let j = 1; j <= nBars; j++)
            boundArr[boundArr.length - j].factor = (nBars - j) / nBars;
        }
        nBars = 1;
      }
    }
    boundArr.push({
      dataIdx:
        minIdx <= maxIdx ||
        mode !== "interpolate" ||
        fftBinInterpolation === "zero insertion"
          ? minIdx
          : minIdx2,
      endIdx: maxIdx,
      factor: 0,
      interpolate:
        minIdx > maxIdx &&
        mode === "interpolate" &&
        fftBinInterpolation !== "zero insertion"
    });
  }
  return boundArr.map((x) => {
    return calcBandpower(
      fftCoeffs,
      x.dataIdx + x.factor,
      x.endIdx,
      average,
      useRMS,
      sum,
      x.interpolate,
      fftBinInterpolation,
      interpParam,
      interpNthRoot,
      interpLogScale
    );
  });
}

/**
 * Brown-Puckette constant-Q transform algorithm, frequency domain filterbank to turn FFT into CQT
 * @param complexFFTCoeffs The complex FFT coefficients to process
 * @param octaveSpacing Fractional octave, higher values provides frequency higher resolution
 * @param constantBandwidth If set to true, the algorithm will be constant-bandwidth instead
 * @param hzArray - Array of center frequency bands
 * @returns Brown-Puckette's CQT output
 */
function calcCQT(
  complexFFTCoeffs,
  octaveSpacing = 12,
  constantBandwidth = false,
  bandwidth = 4,
  windowFunction,
  windowParameter,
  windowSkew,
  hzArray = generateFreqBands(),
  bufferSize = complexFFTCoeffs.length,
  sampleRate = 44100
) {
  return hzArray.map((band) => {
    const freq = band.ctr;
    let l1 = 0,
      l2 = 0,
      r1 = 0,
      r2 = 0,
      a1 = 0,
      a2 = 0,
      b1 = 0,
      b2 = 0,
      real,
      imag;
    let tlen = Math.min(
      constantBandwidth ? Infinity : (1 / freq) * octaveSpacing * 2,
      bufferSize / sampleRate
    );
    let flen = (bandwidth * bufferSize) / (tlen * sampleRate);
    let center = (freq * bufferSize) / sampleRate;
    let start = Math.ceil(center - flen / 2);
    let end = Math.floor(center + flen / 2);
    let len = end - start + 1;

    let coeffs = new Array(len).fill(0);
    for (let i = start; i <= end; i++) {
      let sign = i & 1 ? -1 : 1;
      let x = (2 * (i - center)) / flen;
      let w = applyWindow(x, windowFunction, windowParameter, true, windowSkew);
      coeffs[i - start] = w * sign;
    }
    let L = complexFFTCoeffs.length;
    for (let i = 0; i < coeffs.length; i++) {
      let u = coeffs[i];
      let x = start + i;
      let y = bufferSize - x;
      a1 += u * complexFFTCoeffs[((x % L) + L) % L].re;
      a2 += u * complexFFTCoeffs[((x % L) + L) % L].im;
      b1 += u * complexFFTCoeffs[((y % L) + L) % L].re;
      b2 += u * complexFFTCoeffs[((y % L) + L) % L].im;
    }
    l1 = a1 + b1;
    l2 = a2 - b2;
    r1 = b2 + a2;
    r2 = b1 - a1;
    real = Math.hypot(l1, l2);
    imag = Math.hypot(r1, r2);
    return Math.hypot(real, imag) / 8;
  });
}

function calcAutocorrelation(waveform) {
  return waveform.map((_, i, arr) => {
    let sum = 0;
    for (let j = 0; j < arr.length - i; j++) {
      let lagged = j + i;
      let x = waveform[j];
      let y = waveform[lagged];
      let product = x * y;
      sum += product;
    }

    return sum / arr.length;
  });
}
