// This piece of isolated code came from some code in an html <script> section
// inside the whammy git repo for tests... I guess I'll keep it.
(function () {
  const requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
})();

const framerate = 60;
const capturer = new CCapture({
  format: "webm",
  framerate,
  autoSaveTime: 30,
  verbose: true
});

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

// Yeet, we can show progress of the encoding
const progress = document.getElementById("progress");
const status = document.getElementById("status");
const image = new Image();
const noteblock = new Image();
status.innerHTML = "Choose an mp3 file";

const seed1 = [Math.random() * 42, Math.random() * 42];
const seed2 = [Math.random() * 42, Math.random() * 42];
const seed3 = [Math.random() * 42, Math.random() * 42];



const imageSelector = document.getElementById("image-selector");
imageSelector.addEventListener("change", () => {
    const file = imageSelector.files[0];
    const reader = new FileReader();
    reader.onload = () => image.src = reader.result;
    reader.onerror = (event) => console.error("File Reading Crisis:", event);
    reader.readAsDataURL(file);
  },
  false
);



const noteblockSelector = document.getElementById("noteblock-selector");
noteblockSelector.addEventListener("change", () => {
    const file = noteblockSelector.files[0];
    const reader = new FileReader();
    reader.onload = () => noteblock.src = reader.result;
    reader.onerror = (event) => console.error("File Reading Crisis:", event);
    reader.readAsDataURL(file);
})



const fileSelector = document.getElementById("file-selector");
fileSelector.addEventListener(
  "change",
  () => {
    const file = fileSelector.files[0];
    const reader = new FileReader();
    reader.onload = (event) => {
      const audioCtx = new AudioContext();
      status.innerHTML = "Decoding audio data";
      // Asynchronously decode audio file data contained in an ArrayBuffer.
      audioCtx.decodeAudioData(
        event.target.result,
        (audioBuffer) => {
          const fftSize = 32768;
          const fftHalf = fftSize / 2;
          const step = audioBuffer.sampleRate / framerate;
          progress.max = audioBuffer.duration * framerate;
          const viewport = new Viewport({ image, s: 1.2, a: 20 });
          const vnoteblock = new Viewport({ image: noteblock, s: 0.5, a: 0 });

          status.innerHTML = "Storing PCM data of the first channel";
          const pcmData = audioBuffer.getChannelData(0);

          let index = 0,
            tick = 0,
            waveform = [];
          const dataArray = [];
          status.innerHTML = "Drawing Frames";

          capturer.start();
          function nextFrame() {
            const origoffset = 20; // Offset in milliseconds
            const morigoffset = (origoffset * audioBuffer.sampleRate) / 1000;
            waveform = pcmData.slice(
              index - fftHalf + morigoffset,
              index + fftHalf + morigoffset
            );

            const affordableTick = tick / 50;
            viewport.a = 20 * noise(affordableTick / 2, ...seed1) - 10;
            viewport.x = 50 * noise(affordableTick, ...seed2) - 25;
            viewport.y = 20 * noise(affordableTick, ...seed3) - 10;

            const cw = ctx.canvas.width,
              ch = ctx.canvas.height;
            const hcw = cw >> 1,
              hch = ch >> 1;

            // Background image
            viewport.draw(ctx);

            // Dim the image
            ctx.fillStyle = "#00000030";
            ctx.fillRect(0, 0, cw, ch);
            // Draw the spectrum (duh)
            drawSpectrum(
              ctx,
              audioCtx,
              16384,
              waveform,
              dataArray,
              spectrumSettings,
              barSettings,
              circSettings
            );

            // Draw the noteblock platform
            ctx.fillStyle = "#ffffffcc";
            roundRect(hcw, ch * 0.755, cw / 1.75, hch / 3, cw / 40).fill();
            // Draw the noteblocks
            vnoteblock.y = -hch * 0.675;
            for (let x = -3; x <= 3; x++) {
              vnoteblock.x = x * vnoteblock.image.width * vnoteblock.s;
              vnoteblock.draw(ctx);
            }

            capturer.capture(canvas);
            progress.value++;
            index += step;
            tick++;

            if (index < pcmData.length) {
              requestAnimationFrame(nextFrame);
            } else {
              progress.value = progress.max;
              requestAnimationFrame(finalizeVideo);
            }
          }

          nextFrame();
        },
        (err) => {
          status.innerHTML = "Audio Data Decoding Crisis (check dev console)";
          console.error("Audio Data Decoding Crisis:", err);
        }
      );
    };
    reader.onerror = (event) => console.error("File Reading Crisis:", event);
    reader.readAsArrayBuffer(file);
  },
  false
);

function finalizeVideo() {
  capturer.stop();
  capturer.save();
}
