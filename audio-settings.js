const aps = {
  sampleProvider: "freq kernel",
  hqac: true,
  outputMultiplier: 0,
  inputSize: 8196,
  fftSize: 16384
};

const winTransform = {
  windowFunction: "hann",
  windowParameter: 1,
  windowSkew: 0
};

const winCQT = {
  windowFunction: "hann",
  windowParameter: 1,
  windowSkew: 0
};

const spectrumSettings = {
  spectrumConstantQ: false,
  cqtResolution: 12,
  cqtBandwidth: 4,
  sampleOutCount: 64,
  bandpowerMode: "interpolate",
  fftBinInterpolation: "cubic",
  interpParam: 0,
  interpLogScale: false,
  interpNthRoot: 1,
  useAverage: true,
  useRMS: true,
  useSum: false,
  smoothingTimeConstant: 0,
  perBinFrequencyTilt: 0,
  perBinWeighting: "a",
  perBinWeightingAmount: 0,
  perBandFrequencyTilt: 0,
  perBandWeighting: "a",
  perBandWeightingAmount: 0,
  lowerHz: 20,
  higherHz: 400,
  frequencyScale: "linear",
  hzLinearFactor: 0.5,
  logAmplitudeScale: false,
  dBRange: 70,
  ampNthRoot: 1,
  useAbsValue: true
};

const barSettings = {
  posX: 0,
  posY: 0.5,
  widthMultiplier: 0.5,
  heightMultiplier: 1,
  fixedHeight: 2,
  bumpMultiplier: 1000,
  lineCap: "butt",
  lineJoin: "miter",
  lineMiterLimit: 10,
  width: 0.75,
  drawMode: "bars",
  lineWidth: 1,
  mirror: false
};

const circSettings = {
  circle: false,
  x: 400,
  y: 225,
  a: Math.PI / 2,
  xmin: 0,
  xmax: 800,
  rmin: 100,
  rmul: 0.5
};
